from django.core.cache import cache
import json


def currentUser(request):
    """获取当前用户"""
    # 获取token
    token = request.headers.get("token")
    # 获取当前用户
    user = cache.get(token)
    assert user
    return user


def resetCache(request, user):
    """重置用户缓存"""
    token = request.headers.get("token")
    cache.set(token, user)


def tostr(obj):
    """将json对象转化为字符串"""
    return json.dumps(obj, ensure_ascii=False).replace('"', "")
