from django.contrib.auth.hashers import check_password
from django.core.cache import cache
from django.core.paginator import Paginator
from django.utils.decorators import method_decorator
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from utils.permissions import userValidate, AdminPermission
from utils.tools import currentUser, resetCache, tostr
from v1.serializers import *
from v1.models import *

import uuid

# Create your views here.


@method_decorator(userValidate, "get")
class UserView(APIView):
    # 管理员权限
    def get(self, request):
        """获取所有学生信息"""
        kwargs = {"isadmin": False}
        # 根据名字搜索学生
        params = request.query_params
        name = params.get("name")
        if name:
            kwargs["name__contains"] = name
        users = User.objects.filter(**kwargs).all()
        us = UserSerializer(instance=users, many=True)
        return Response({"code": 200, "ulist": us.data})

    def patch(self, request):
        """修改用户信息"""
        user = currentUser(request)
        if not user:
            return Response({"code": 401, "msg": "用户暂未登录"})
        # 获取表单数据
        data = request.data
        # 反序列化
        us = UserSerializer(instance=user, data=data, partial=True)
        if us.is_valid():
            user = us.save()
            resetCache(request, user)
            return Response({"code": 200, "msg": "修改成功"})
        else:
            return Response({"code": 10001, "msg": tostr(us.errors)})


class UserManageView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AdminPermission]


class RegisterView(APIView):
    def post(self, request):
        """注册用户"""
        # 获取表单数据
        data = request.data
        # 反序列化
        us = UserSerializer(data=data)
        if us.is_valid():
            us.save()
            return Response({"code": 200, "msg": f"注册成功<{us.data['mobile']}>"})
        else:
            return Response({"code": 10001, "msg": tostr(us.errors)})

    def patch(self, request):
        """忘记密码"""
        data = request.data
        mobile = data.get("mobile")
        user = User.objects.filter(mobile=mobile).first()
        if user:
            us = UserSerializer(instance=user, data=data, partial=True)
            if us.is_valid():
                user = us.save()
                resetCache(request, user)
                return Response({"code": 200, "msg": f"修改成功"})
            else:
                return Response({"code": 10001, "msg": tostr(us.errors)})
        else:
            return Response({"code": 404, "msg": "手机号尚未注册"})


class LoginView(APIView):
    def post(self, request):
        """用户登录"""
        data = request.data
        mobile = data.get("mobile")
        user = User.objects.filter(mobile=mobile).first()
        if user:
            passwd = data.get("passwd")
            # 验证密码
            if check_password(passwd, user.passwd):
                logined = cache.get("logined")
                # 生成token
                token = str(uuid.uuid1())
                if logined:
                    if user.id in logined:  # 已登录，返回token
                        token = logined[user.id]
                    else:  # 未登录，设置token缓存
                        cache.set(token, user)
                        # 加入已登录列表
                        logined[user.id] = token
                        cache.set("logined", logined)
                else:  # 初始化已登录列表缓存
                    cache.set(token, user)
                    cache.set("logined", {user.id: token})
                return Response(
                    {
                        "code": 200,
                        "msg": f"用户<{user.name}>登录成功",
                        "token": token,
                        "name": user.name,
                        "isadmin": user.isadmin,
                    }
                )
            else:
                return Response({"code": 10001, "msg": "密码错误"})
        else:
            return Response({"code": 404, "msg": "用户不存在"})


@method_decorator(userValidate, "post")
class QuestionView(APIView):
    queryset = Question.objects.order_by("category__id", "id").all()

    def get(self, request):
        """获取所有题目"""
        queryset = self.queryset
        # 获取查询参数
        params = request.query_params

        # 用户是学生，获取已经背会或未背会的题目
        user = currentUser(request)
        if not user:
            return Response({"code": 400, "msg": "用户暂未登陆"})
        if not user.isadmin:
            if params.get("islearn") == "1":
                queryset = user.questions.all()
            else:
                ids = [i.id for i in user.questions.all()]
                queryset = queryset.exclude(id__in=ids).all()

        # 搜索
        search = {}

        # 根据分类查询
        category_id = params.get("category_id")
        if category_id:
            search["category_id"] = category_id
        # 根据关键字查询
        keyword = params.get("keyword")
        if keyword:
            search["title__contains"] = keyword
        queryset = queryset.filter(**search).all()

        # 分页
        page_size = 10
        current = int(params.get("page", 1))  # 当前页码
        paginator = Paginator(queryset, page_size)
        # 没有页码或页码错误，返回第一页
        if not current or current > paginator.num_pages:
            current = 1
        page = paginator.page(current)  # 当前页
        # page_range = list(paginator.page_range)  # 所有页码
        count = paginator.count  # 数据总数

        # 序列化
        qs = QuestionSerializer(instance=page, many=True)

        return Response(
            {
                "code": 200,
                "qlist": qs.data,
                "count": count,
                "current": current,
            }
        )

    def post(self, request):
        """添加题目"""
        # 获取表单数据
        data = request.data
        # 反序列化
        qs = QuestionSerializer(data=data)
        if qs.is_valid():
            qs.save()
            return Response({"code": 200, "msg": "添加成功"})
        else:
            return Response({"code": 10001, "msg": tostr(qs.errors)})


class QuestionManageView(RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [AdminPermission]


@method_decorator(userValidate, "post")
class CategoryView(APIView):
    queryset = Category.objects.all()

    def get(self, request):
        """获取所有分类"""
        queryset = self.queryset
        # 获取未删除的分类
        queryset = queryset.filter(isdelete=0).all()
        cs = CategorySerializer(instance=queryset, many=True)
        return Response({"code": 200, "clist": cs.data})

    def post(self, request):
        """添加题目分类"""
        data = request.data
        cs = CategorySerializer(data=data)
        if cs.is_valid():
            cs.save()
            return Response({"code": 200, "msg": f"添加分类<{cs.data.get('name')}>成功"})
        else:
            return Response({"code": 10001, "msg": tostr(cs.errors)})


class CategoryManageView(RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [AdminPermission]


@method_decorator(userValidate, "post")
class UserQuestionView(APIView):
    def get(self, request):
        pass

    # 用户权限
    def post(self, request):
        user = currentUser(request)
        # 获取题目
        question_id = request.data.get("question_id")
        op = request.data.get("op")
        question = Question.objects.filter(id=question_id).first()
        if question:
            if op == "1":  # 已学会
                user.questions.add(question)
            else:  # 未背会
                user.questions.remove(question)
            resetCache(request, user)
            return Response({"code": 200})
        else:
            return Response({"code": 404, "msg": f"题目<{question_id}>不存在"})
