from django.urls import path
from .views import *

urlpatterns = [
    path("user/", UserView.as_view()),
    path("user/<int:pk>", UserManageView.as_view()),
    path("register/", RegisterView.as_view()),
    path("login/", LoginView.as_view()),
    path("question/", QuestionView.as_view()),
    path("question/<int:pk>", QuestionManageView.as_view()),
    path("category/", CategoryView.as_view()),
    path("category/<int:pk>", CategoryManageView.as_view()),
    path("userquestion/", UserQuestionView.as_view()),
]
