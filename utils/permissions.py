from rest_framework.response import Response
from rest_framework.permissions import BasePermission
from utils.tools import currentUser

"""
所有角色权限：注册，登录，查看所有题目，个人信息管理
用户权限：修改题目状态(学会/未学会)
管理员权限：添加分类，添加题目，查看所有用户信息，管理分类和题目(修改/删除)
"""


class AdminPermission(BasePermission):
    """
    管理员权限
    管理分类和题目(修改/删除)
    """

    def has_permission(self, request, view):
        user = currentUser(request)
        return user.isadmin


def userValidate(func):
    """用户身份及请求地址验证"""
    # 用户权限：修改题目状态(学会/未学会)
    user_path = ["/v1/" + i for i in ["userquestion/"]]
    # 管理员权限：添加分类，添加题目，查看所有用户信息
    admin_path = ["/v1/" + i for i in ["user/", "category/", "question/"]]

    def wrapper(request, *args, **kwargs):
        user = currentUser(request)
        path = request.path
        if user:
            if user.isadmin and path in admin_path:
                return func(request, *args, **kwargs)
            if not user.isadmin and path in user_path:
                return func(request, *args, **kwargs)
            return Response(
                {"code": 403, "msg": f"当前用户没有访问权限<({path},{request.method})>"}
            )
        else:
            return Response({"code": 401, "msg": "用户暂未登录"})

    return wrapper
