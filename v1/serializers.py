import re
from django.contrib.auth.hashers import make_password
from rest_framework import serializers as s

from .models import *


class CategorySerializer(s.ModelSerializer):
    isdelete = s.BooleanField(write_only=True, default=False)

    class Meta:
        model = Category
        fields = "__all__"


class QuestionSerializer(s.ModelSerializer):
    category = CategorySerializer(read_only=True)
    category_id = s.IntegerField(write_only=True)

    def validate_title(self, data):
        if not self.instance:
            if Question.objects.filter(title=data).first():
                raise s.ValidationError(detail="题目已存在")
        return data

    class Meta:
        model = Question
        fields = "__all__"


class UserSerializer(s.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)
    passwd = s.CharField(write_only=True)

    def validate_mobile(self, data):
        if re.match(r"^1[3-9]\d{9}$", data):
            user = User.objects.filter(mobile=data).first()
            ins = self.instance
            # 手机号没有被注册或是当前用户，跳过验证
            if not user or ins == user:
                return data
            else:
                raise s.ValidationError(detail=f"手机号<{data}>已被注册")
        else:
            raise s.ValidationError(detail=f"手机号<{data}>错误")

    def validate_passwd(self, data):
        if not re.match(r"^[0-9a-zA-Z]{6,14}$", data):
            raise s.ValidationError(detail=f"密码应由6-14位数字字母组成")
        return make_password(data)

    class Meta:
        model = User
        fields = "__all__"
