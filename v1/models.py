from django.db import models


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name="分类名称")
    isdelete = models.BooleanField(default=0, verbose_name="逻辑删除")

    class Meta:
        db_table = "Category".lower()


class Question(models.Model):
    title = models.CharField(verbose_name="标题", max_length=255)
    content = models.TextField(verbose_name="内容")
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="questions"
    )

    class Meta:
        db_table = "Question".lower()


class User(models.Model):
    name = models.CharField(max_length=20, verbose_name="姓名")
    mobile = models.CharField(max_length=11, verbose_name="手机号")
    passwd = models.CharField(max_length=200, verbose_name="密码")
    questions = models.ManyToManyField(Question, related_name="users")
    isadmin = models.BooleanField(default=0, verbose_name="是否管理员")

    class Meta:
        db_table = "User".lower()
