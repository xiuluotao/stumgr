from django.utils.deprecation import MiddlewareMixin
from django.core.cache import cache
from django.http import JsonResponse


class PermissionMiddleware(MiddlewareMixin):
    def process_request(self, request):
        white_list = ["/v1/login/", "/v1/register/"]
        if request.path in white_list:
            return

        token = request.headers.get("token")
        if not token:
            return JsonResponse({"code": 401, "msg": "必须传递token"})
        if not cache.get(token):
            return JsonResponse({"code": 400, "msg": "用户暂未登录"})
        cache.touch(token)
